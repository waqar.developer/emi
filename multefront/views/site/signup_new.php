
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \multeback\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use Yii;

$this->title = Yii::t('app', 'Signup');

?>

<!DOCTYPE html>
<html>

<head>


    <style id="compiled-css" type="text/css">
        .menu-text{
            line-height: 7px !important;
        }

        .menu-text {
            color: #fff !important;
        }

        a.nav-link {
            color: #fff !important;
        }

        .navbar-light .navbar-nav .active>.nav-link {
            color: rgb(255, 255, 255) !important;
        }

        /*.form-control {
            display: block;
            height: 36px;
            width: 100%;
            border: none;
            border-radius: 0 !important;
            font-size: 16px;
            font-weight: 300;
            padding: 0;
            background-color: transparent;
            box-shadow: none;
            border-bottom: 1px solid #757575;
        }*/

        .send-code {
            background-color: rgba(255, 190, 123, 1.0) !important;
            font-family: "AzoSans-Bold", Helvetica, Arial, serif;
            font-size: 11.0px;
            color: rgba(255, 255, 255, 1.0);
            text-align: left;
            letter-spacing: 2.18px;
            line-height: 17.0px;
            position: absolute;
            right: 0;
            top: -20px;
            padding-left: 1rem;
            padding-right: 1rem;
        }

        /*.log{
            text-align:center;
        }
        .form {
            margin: 0 auto !important;
            position: relative;
            top: 50%;

            width: 337px;

        }
        .form-control, .input-group-addon {
            border-radius: 4px;
        }*/


        /*input[type="button"] {
            margin-left: -50px;
            height: 25px;
            width: 50px;

            color: white;
            border: 0;
            -webkit-appearance: none;
        }
        input.sndmail {
            position: absolute;
            top: 178px;
            left: 93%;
            width: 20%;
        }
        input.sndotp {
            position: absolute;
            top: 424px;
            left: 93%;
            width: 20%;
        }

        input#verify {
            height: 24px;
            width: 44px;
        }*/

        .card {

            color: red;
        }


        .button-container {
            text-align: center;
        }

        select#signupform-state {
            border-radius: 5px !important;
        }

        .field-icon {
            float: right;
            margin-left: -25px;
            margin-top: -25px;
            position: relative;
            z-index: 2;
            color: #000;
        }

/*
        .form-group input:lang(ur), .form-group textarea:lang(ur){
            text-align: right;
        }*/

    </style>
</head>

<body>
<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    <div class="loan-form signup-form">
        <section class="py-5 sec-loanform signup-page">
            <div class="container">
                <div class="row">
                    <div class="col-4 float-left text-top">
                        <h2 class="whetheryouhaveac"><?=Yii::t('app','Smart Loan')?><br><?=Yii::t('app','for')?>&nbsp;<?=Yii::t('app','Smart Phones')?>
                        </h2>
                    </div>
                    <div class="col-6 float-right img-top-signup">
                        <img src="<?=Yii::$app->homeUrl?>img/sign-up-group-421.png">
                    </div>
                </div>
                <div class="box-shad-light card card-sign card-signup">
                    <div class="card-body">
                        <form>


                            <div class="col-12">

                                <?= $form->field($model, 'firstname', [
                                    'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i>{error}{hint}',
                                    'labelOptions' => ['class' => 'control-label font']
                                ])->textInput([ 'class' => 'font']); ?>
                            </div>

                            <div class="col-12">

                                <?= $form->field($model, 'lastname', [
                                    'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i>{error}{hint}',
                                    'labelOptions' => ['class' => 'control-label font']
                                ])->textInput([ 'class' => 'font']); ?>
                            </div>


                            <div class="col-12">

                                <?= $form->field($model, 'email', [
                                    'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i>{error}{hint}',
                                    'labelOptions' => ['class' => 'control-label font']
                                ])->textInput(['id'=> 'email', 'class' => 'font']); ?>
                            </div>

                            <div class="col-12">

                                <?= $form->field($model, 'cnic', [
                                    'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i>{error}{hint}',
                                    'labelOptions' => ['class' => 'control-label font']
                                ])->textInput(['id'=>'cnic', 'class' => 'font']); ?>
                            </div>


                            <div class="col-12">

                                <?= $form->field($model, 'mobile', [
                                    'template' => '{input}<span class="input-group-addon"><span class=" btn-lg btn-primary details-btn mr-2 send-code font" id="mobsend" onclick="sendOTP();">'.Yii::t("app","SEND CODE").'</span></span>{beginLabel}{labelTitle}{endLabel}<i class="bar"></i>{error}{hint}',
                                    'labelOptions' => ['class' => 'control-label font']
                                ])->textInput(['id' => 'mobile', 'class' => 'font']); ?>
                            </div>

                            <div class="col-12">

                                <?= $form->field($model, 'mobotp', [
                                    'template' => '{input}<span class="input-group-addon"><span id="mobverify" class="btn-lg btn-primary details-btn mr-2 send-code font font" onclick="verifyOTP();">'.Yii::t("app","Verify").'</span></span>{beginLabel}{labelTitle}{endLabel}<i class="bar"></i>{error}{hint}',
                                    'labelOptions' => ['class' => 'control-label font']
                                ])->textInput(['id' => 'mobileOtp', 'class' => 'font']); ?>
                            </div>



                            <div class="col-12">
                                <?= $form->field($model, 'password', [
                                    'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i> <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>{error}{hint}',
                                    'labelOptions' => ['class' => 'control-label font']
                                ])->passwordInput(['class' => 'font','id'=>'password-field']); ?>
                            </div>

                            <div class="col-12">
                                <?= $form->field($model, 'state', [
                                    'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i> {error}{hint}',
                                    'labelOptions' => ['class' => 'control-label font']
                                ])->dropDownList(['4261' => Yii::t('app','AZAD KASHMIR'), '4262' => Yii::t('app','BALOCHISTAN'),'4263'=>Yii::t('app','FEDERALLY ADMINISTERED TRIBAL AREAS'),'4264'=>Yii::t('app','ISLAMABAD'),'4265'=>Yii::t('app','KHYBER PAKHTUNKHWA'),'4266'=>Yii::t('app','NORTHERN AREAS'),'4267'=>Yii::t('app','PUNJAB'),'4268'=>Yii::t('app','SINDH')],['prompt'=>Yii::t('app','Select Province'),'class' => 'font']); ?>
                            </div>

                            <div class="col-12">

                                <?= $form->field($model, 'address', [
                                    'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i>{error}{hint}',
                                    'labelOptions' => ['class' => 'control-label font']
                                ])->textarea([ 'class' => 'font']); ?>
                            </div>



                            <div class="col-12">
                                <div class="form-group font">
                                    <div class="font "  id="error" style="color: red;text-align: center"> </div>
                                </div>
                            </div>

                        </form>

                        <div class="col-12">
                            <div class="button-container font">
                               <!-- <span  id="beforesubmit" class="button m-0" name="signup-button"><?/*=Yii::t('app', '<span>Signup</span>')*/?></span>-->
                                <?= Html::submitButton(Yii::t('app', '<span>Signup</span>'), ['class' => 'button m-0', 'name' => 'signup-button','id'=>'signsubmit']) ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>





<?php ActiveForm::end(); ?>
</body>

</html>
<script>

  //  $('#signsubmit').hide();
    $('#beforesubmit').click(function () {
        $('#error').empty();
        $('#error').append('<?=Yii::t("app","Please verify your mobile number")?>');
    });

    function startTimer(){
        var counter = 60;
        setInterval(function() {
            counter--;
            if (counter >= 0) {
                $("#mobsend").empty();
                span = document.getElementById("mobsend");
                span.innerHTML = counter;
                $("#mobsend").css("pointer-events", "none");
            }
            if (counter === 0) {
                $("#mobsend").empty();
                $("#mobsend").append('<?=Yii::t("app","Resend Code")?>');
                $("#mobsend").css("pointer-events", "auto");
            }
        }, 1000);
    }

    $(".toggle-password").click(function() {

        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
    $("input").prop('required',true);
    $('#cnic').attr('onkeypress',"return isNumeric(event)");
    $('#cnic').attr('oninput',"maxLengthCheck(this)");




  function maxLengthCheck(object) {
      if (object.value.length > 13)
          object.value = object.value.slice(0, object.maxLength)
  }

  function isNumeric (evt) {
      var theEvent = evt || window.event;
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode (key);
      var regex = /[0-9]|\./;
      if ( !regex.test(key) ) {
          theEvent.returnValue = false;
          if(theEvent.preventDefault) theEvent.preventDefault();
      }
  }

</script>




