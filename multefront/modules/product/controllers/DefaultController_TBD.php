<?php

namespace multefront\modules\product\controllers;

use multebox\Controller;
use multebox\models\Inventory;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `product` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

	public function actionDetail()
    {
		//var_dump($_POST);exit;
		if(isset($_POST['attribute_value']))
		{
			$itemsList = Inventory::find()
									->where("product_id=".$_POST['product_id'])
									->andWhere("attribute_values='".json_encode($_POST['attribute_value'])."'")
									->all();

			return $this->render('listing', [
            'itemsList' => $itemsList
			]);
		}
		else
		{
			$inventory = Inventory::findOne($_GET['inventory_id']);
		}

        return $this->render('detail', [
            'inventory' => $inventory
        ]);
    }

	public function actionListing()
    {
		if($_GET['category_id'] != '' && $_GET['sub_category_id'] != '' && $_GET['sub_subcategory_id'] != '')
		{
			$itemsList = Inventory::find()
								->joinWith('inventoryProducts p')
								->where('p.category_id = '.$_GET['category_id'])
								->andWhere('p.sub_category_id = '.$_GET['sub_category_id'])
								->andWhere('p.sub_subcategory_id = '.$_GET['sub_subcategory_id'])
								->orderBy('stock desc, name')
								->asArray()
								->all();
		}
		else
		if($_GET['category_id'] != '' && $_GET['sub_category_id'] != '')
		{
			$itemsList = Inventory::find()
								->joinWith('inventoryProducts p')
								->where('p.category_id = '.$_GET['category_id'])
								->andWhere('p.sub_category_id = '.$_GET['sub_category_id'])
								->orderBy('stock desc, name')
								->asArray()
								->all();
		}
		else
		if($_GET['category_id'] != '')
		{
			$itemsList = Inventory::find()
								->joinWith('inventoryProducts p')
								->where('p.category_id = '.$_GET['category_id'])
								->orderBy('stock desc, name')
								->all();
		}
		else
		{
			throw new NotFoundHttpException(Yii::t('app', 'You are trying to perform an activity which is not allowed!'));
		}

        return $this->render('listing', [
            'itemsList' => $itemsList
        ]);
    }
}
