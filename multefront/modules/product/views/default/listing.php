<?php
use multebox\models\File;
use multebox\models\Vendor;
use multebox\models\search\MulteModel;
use multebox\models\ProductCategory;
use multebox\models\ProductSubCategory;
use multebox\models\ProductSubSubCategory;
use yii\helpers\Url;
use yii\helpers\Html;

include_once('../web/cart_script.php');
?>

<script type="text/javascript" src="<?=Url::base()?>/js/jquery-2.1.1.min.js"></script>

<script>
$(document).ready(function(){

 $(".mylazy").lazyload({
        event : "turnPage",
        effect : "fadeIn"
    });

$("div.holder").jPages({
        containerID : "itemContainer",
		perPage		: 20,
        animation   : "fadeInUp",
        callback    : function( pages,
        items ){
            /* lazy load current images */
        items.showing.find("img").trigger("turnPage");
        /* lazy load next page images */
        items.oncoming.find("img").trigger("turnPage");
        }
    });
/*
$(".mygrid").click(function(){
	$("div.holder").jPages("destroy");
	setTimeout(function(){ 
		$("div.holder").jPages({
        containerID : "itemContainer",
		perPage		: 1,
        animation   : "fadeInUp",
        callback    : function( pages,
        items ){
        items.showing.find("img").trigger("turnPage");
        items.oncoming.find("img").trigger("turnPage");
        }
    });
		}, 1);
	});

$(".mylist").click(function(){
	$("div.holder").jPages("destroy");
	setTimeout(function(){ 
		$("div.holder").jPages({
        containerID : "itemContainer",
		perPage		: 1,
        animation   : "fadeInUp",
        callback    : function( pages,
        items ){
        items.showing.find("img").trigger("turnPage");
        items.oncoming.find("img").trigger("turnPage");
        }
    });
		}, 1);
	});
*/

$('#categoryfilter').change(function(){
	$('#subcategoryfilter').html('<option value="">--<?=Yii::t('app', 'Select')?>--</option>');
	$('#childcategoryfilter').html('<option value="">--<?=Yii::t('app', 'Select')?>--</option>');
   $.post("<?=Url::to(['/product/default/ajax-load-sub-category'])?>", { 'category_id': $(this).val()}) .done(function(result){
					$('#subcategoryfilter').html(result);
					$('#childcategoryfilter').html('<option value="">--<?=Yii::t('app', 'Select')?>--</option>');
				})
	})

$('#subcategoryfilter').change(function(){
	$('#childcategoryfilter').html('<option value="">--<?=Yii::t('app', 'Select')?>--</option>');
    $.post("<?=Url::to(['/product/default/ajax-load-sub-sub-category'])?>", { 'sub_category_id': $(this).val()}) .done(function(result){
					$('#childcategoryfilter').html(result);
				})
	})

$('#subcategoryfilter').load("<?=Url::to(['/product/default/ajax-load-sub-category', 'category_id' => $category_id, 'sub_category_id' => $sub_category_id])?>");

$('#childcategoryfilter').load("<?=Url::to(['/product/default/ajax-load-sub-sub-category', 'sub_category_id' => $sub_category_id, 'sub_subcategory_id' => $sub_subcategory_id])?>");

});
</script>
<div class="container">
  <div id="container">
    
      <!-- Breadcrumb Start-->
      <ul class="breadcrumb">
	    <?php
	     $url1 = Url::to(['/site/index']);
		 $url2 = Url::to(['/product/default/listing', 'category_id' => $_GET['category_id']]);
		 $url3 = Url::to(['/product/default/listing', 'category_id' => $_GET['category_id'], 'sub_category_id' => $_GET['sub_category_id']]);
		 $url4 = Url::to(['/product/default/listing', 'category_id' => $_GET['category_id'], 'sub_category_id' => $_GET['sub_category_id'], 'sub_subcategory_id' => $_GET['sub_subcategory_id']]);
	    ?>
        <li><a href="<?=$url1?>"><i class="fa fa-home"></i></a></li>
		<?php
		$result_label = Yii::t('app', 'Result');
		/*switch ($listing_type)
		{
			case 'search':
				$result_label = Yii::t('app', 'Result');
				break;

			case 'filter_search':
				$result_label = Yii::t('app', 'Matching Products');
				break;

			case 'vendor_search':
				$vendor = Vendor::findOne($vendor_id);
				$result_label = Yii::t('app', 'Products By Vendor:').' '.$vendor->vendor_name;
				break;
		}*/

		if($_GET['category_id'] != '')
		{
		?>
		<li><a href="<?=$url2?>"><?=ProductCategory::findOne($_GET['category_id'])->name?></a></li>
		<?php
		}
		?>
		<?php
		if($_GET['category_id'] != '' && $_GET['sub_category_id'] != '')
		{
		?>
		<li><a href="<?=$url3?>"><?=ProductSubCategory::findOne($_GET['sub_category_id'])->name?></a></li>
		<?php
		}
		?>
		<?php
		if($_GET['category_id'] != '' && $_GET['sub_category_id'] != '' && $_GET['sub_subcategory_id'] != '')
		{
		?>
		<li><a href="<?=$url4?>"><?=ProductSubSubCategory::findOne($_GET['sub_subcategory_id'])->name?></a></li>
		<?php
		}
		?>

		<li><?=$result_label?></li>

      </ul>
      <!-- Breadcrumb End-->
      <div class="row">
        <!--Middle Part Start-->
        <div id="content" class="col-sm-12">
         <!-- <h1 class="title"><?=ProductCategory::findOne($_GET['category_id'])->name?></h1>-->
		 <?php
		 if($showfilters == 'true')
		 {
		 ?>
		  <div class="product-filter">
            <div class="row">
			  <div class="col-md-12 col-sm-12 text-right">
				<button class="btn-info" data-toggle="collapse" data-target="#collapsible"><?=Yii::t('app', 'Show/Hide Options')?></button>
              </div>
			</div>
		  </div>
			
		  <div class="collapse in" id="collapsible">
				<!-- Start -->
			<form method="post" action="<?=Url::to(['/product/default/filter'])?>">
			  <div class="product-filter">

				<div class="row">

				   <div class="col-md-2 col-sm-2 text-left"><?=Yii::t('app', 'Sort By')?>
						<select id="sortfilter" name="sortfilter" class="form-control col-sm-3">
						  <option value="" selected="selected">--<?=Yii::t('app', 'Select')?>--</option>
						  <option value="name_asc" <?=$sortfilter=='name_asc'?'selected':''?>><?=Yii::t('app', 'Name (A - Z)')?></option>
						  <option value="name_desc" <?=$sortfilter=='name_desc'?'selected':''?>><?=Yii::t('app', 'Name (Z - A)')?></option>
						  <option value="price_asc" <?=$sortfilter=='price_asc'?'selected':''?>><?=Yii::t('app', 'Price (Low to High)')?></option>
						  <option value="price_desc" <?=$sortfilter=='price_desc'?'selected':''?>><?=Yii::t('app', 'Price (High to Low)')?></option>
						</select>
				  </div>

				  <div class="col-md-2 col-sm-2 text-left"><?=Yii::t('app', 'Products Type')?>
						<select id="digitaltype" name="digitaltype" class="form-control col-sm-3">
						  <option value="" selected="selected">--<?=Yii::t('app', 'Select')?>--</option>
						  <option value="1" <?=$digital=='1'?'selected':''?>><?=Yii::t('app', 'Digital')?></option>
						  <option value="0" <?=$digital=='0'?'selected':''?>><?=Yii::t('app', 'Non-Digital')?></option>
						</select>
				  </div>

				  <div class="col-md-2 col-sm-2 text-left"><?=Yii::t('app', 'Category')?>
						<select id="categoryfilter" name="category_id" class="form-control col-sm-3">
						  <option value="" selected="selected">--<?=Yii::t('app', 'Select')?>--</option>
						  <?php
						  $categories = ProductCategory::find()->where("active=1 order by name")->all();
						  foreach ($categories as $row)
						  {
						  ?>
							<option value="<?=$row->id?>" <?=$category_id==$row->id?'selected':''?>><?=$row->name?></option>
						  <?php
						  }
						  ?>
						</select>
				  </div>

				  <div class="col-md-2 col-sm-2 text-left"><?=Yii::t('app', 'Sub Category')?>
						<select id="subcategoryfilter" name="sub_category_id" class="form-control col-sm-3">
						  <option value="" selected="selected">--<?=Yii::t('app', 'Select')?>--</option>
						</select>
				  </div>

				  <div class="col-md-2 col-sm-2 text-left"><?=Yii::t('app', 'Child Category')?>
						<select id="childcategoryfilter" name="sub_subcategory_id" class="form-control col-sm-3">
						  <option value="" selected="selected">--<?=Yii::t('app', 'Select')?>--</option>
						</select>
				  </div>

				  <div class="col-md-2 col-sm-2 text-left"><?=Yii::t('app', 'Vendor')?>
						<select id="vendorfilter" name="vendor_id" class="form-control col-sm-3">
						  <option value="" selected="selected">--<?=Yii::t('app', 'Select')?>--</option>
						  <?php
						  $vendors = Vendor::find()->where("active=1 order by vendor_name")->all();
						  foreach ($vendors as $row)
						  {
						  ?>
							<option value="<?=$row->id?>" <?=$vendor_id==$row->id?'selected':''?>><?=$row->vendor_name?></option>
						  <?php
						  }
						  ?>
						</select>
				  </div>
				</div> <!-- row -->

			  </div> <!-- product filter -->

			  <div class="product-filter">
				<div class="row">
				  <div class="col-md-12 col-sm-12 text-right">
					<button class="btn-primary" type="submit"><?=Yii::t('app', 'Apply Selected Filter')?></button>
				  </div>
				</div>
			  </div>
			</form>
				<!-- End -->
		  </div>
		  <?php
		  }
		  ?>
		  <div class="visible-xs" id="xs-check"></div>
		  <script>
			if( $("#xs-check").is(":visible") )
				$("#collapsible").removeClass("in");
		  </script>

          
          <br />
		  <div class="holder"></div>
		  <div class="row products-category" id="itemContainer">
		    <?php
			
//var_dump($itemsList);
			foreach($itemsList as $inventoryItem)
			{
				$inventoryPrice = floatval($inventoryItem['price']);
				if($inventoryItem['price_type'] == 'B')
				{
					foreach(json_decode($inventoryItem['attribute_price']) as $row)
					{
						$inventoryPrice += floatval($row);
					}
				}
	
			    if($inventoryItem['discount_type'] == 'P')
				  $inventoryDiscount = $inventoryItem['discount'];
			    else
				{
				  if($inventoryPrice > 0)
					  $inventoryDiscount = round((floatval($inventoryItem['discount'])/$inventoryPrice)*100,2);
				  else
					  $inventoryDiscount = 0;
				}

			    $inventoryDiscountedPrice = round($inventoryPrice - $inventoryPrice*$inventoryDiscount/100, 2);

			    $fileDetails = File::find()->where("entity_type='product' and entity_id=".$inventoryItem['product_id'])->one();
		    ?>
            <!--<div class="product-layout product-list col-xs-12">-->
			<div class="product-layout product-grid col-lg-5ths col-md-5ths col-sm-3 col-xs-12">
              <div class="product-thumb">
			  <?php
				 $url1 = Url::to(['/product/default/detail', 'inventory_id' => $inventoryItem['id']]);
			  ?>
                <div class="image"><a href="<?=$url1?>"><img src="<?=Url::base()?>/image/no_image.jpg" data-src ="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" alt="<?=$inventoryItem['product_name']?>" title="<?=$inventoryItem['product_name']?>" class="img-responsive mylazy img-listing" /></a></div>
              
                  <div class="caption">
                    <h4><a href="product.html"> <?=$inventoryItem['product_name']?> </a></h4>
                    <p class="description"> </p>
                    <p class="price"> 
						<span class="price-new"><?=MulteModel::formatAmount($inventoryDiscountedPrice)?></span> 
						<?php
						if($inventoryDiscountedPrice != $inventoryPrice)
						{
						?>
						<span class="price-old"><?=MulteModel::formatAmount($inventoryPrice)?></span> 
						<span class="saving">-<?=$inventoryDiscount?>%</span> 
						<?php
						}
						?>
					</p>
					<p> <input type="text" class="multe-rating-nocap-sm" value="<?=$inventoryItem['product_rating']?>" readonly> </p>
                  </div>
                  <div class="button-group">
                    <?php
					if($inventoryItem['stock'] > 0)
					{
					?>
                        <a href="<?=$url1?>"><button class="btn-primary " type="button" value="<?=$inventoryItem['id']?>"><span><?=Yii::t('app', 'Buy Now')?></span></button></a>

                    <!--<button class="btn-primary addtocart" type="button" value="<?/*=$inventoryItem['id']*/?>"><span><?/*=Yii::t('app', 'Add to Cart')*/?></span></button>
                    --><?php
					}
					else
					{
					?>
					<button class="btn-primary" type="button" onClick="" disabled><span><?=Yii::t('app', 'Out of Stock')?></span></button>
					<?php
					}
					?>

                  </div>
               
              </div>
            </div>
			<?php
			}
			?>
          </div>
		  <div class="holder"></div>
          
        </div>
        <!--Middle Part End -->
      </div>
    
  </div>
</div>
