<?php

namespace multefront\modules\payments\controllers;

use Faker\Provider\hr_HR\Payment;
use Yii;
use multebox\models\Payments;
use multebox\models\search\PaymentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PaymentsController implements the CRUD actions for Payments model.
 */
class PaymentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*public function behaviors()
    {

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='lead-status/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ],
        ];

        return $behaviors;
    }*/

    /**
     * Lists all Payments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentsSearch();
        $dataProvider = $searchModel->search2(Yii::$app->request->queryParams);

        return $this->render('payment_index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Payments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payments();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function  actionCreateVochers($s_id,$o_id){

        $oItem = \multebox\models\SubOrder::find()->where(['=','id',$s_id])->one();
        $c_id =  \multebox\models\Order::find()->where(['=','id',$o_id])->one()->customer_id;


        $query = \multebox\models\PaymentPlanTemplateDetail::find()->where(['=','payment_plan_template_id',$oItem->plan_id])->orderBy(['id'=>'ASC'])->all();
        /* echo '<pre>';
         echo  print_r($query);
         echo '</pre>';*/

        $count = 0;
        $total =0;

        foreach ($query as $val){

            if($val->format == 'rs'){
                if($val->type == 'initial'){

                    $due_date = date('Y-m-29');


                    $this->actionSaveVocher($c_id,$o_id,$s_id,$oItem->plan_id,$val->payment_amount,$count,$due_date,$oItem->payment_method);

                }
                else
                {
                    $today = date('Y-m-11');
                    $due_date = date('Y-m-d', strtotime($today . "+".$count." months"));
                    $total +=  $val->payment_amount;
                }
                $this->actionSaveVocher($c_id,$o_id,$s_id,$oItem->plan_id,$val->payment_amount,$count,$due_date,$oItem->payment_method);
            }
            else
                {
                if($val->type == 'initial'){
                    $due_date = date('Y-m-29');
                    $amount = $oItem->total_cost *$val->payment_amount /100;
                    $this->actionSaveVocher($c_id,$o_id,$s_id,$oItem->plan_id,$amount,$count,$due_date,$oItem->payment_method);
                }
                else
                {
                    $today = date('Y-m-11');
                    $due_date = date('Y-m-d', strtotime($today . "+".$count." months"));
                    $amount = $oItem->total_cost * $val->payment_amount /100;
                    $this->actionSaveVocher($c_id,$o_id,$s_id,$oItem->plan_id,$amount,$count,$due_date,$oItem->payment_method);
                }
            }
            $count++;
        }

        $order = \multebox\models\SubOrder::findOne($s_id);
        $order->sub_order_status = 'CNF';
        if($order->save()){

            return $this->redirect('site/index');
        }

    }

    protected function actionSaveVocher($c_id,$o_id,$s_id,$p_id,$amount,$count,$due_date,$payment_method){

        $model = new Payments();
        $model->customer_id = $c_id;
        $model->order_id = $o_id;
        $model->sub_order_id = $s_id;
        //$model->payment_method = $payment_method;
        $model->plan_id = $p_id;
        $model->due_date = $due_date;
        $model->installment_no = $count;
        $model->installment_amonut = $amount;
        $model->status = 0;
        $model->created_at = date("Y-m-d H:i:s");
        $model->created_by = 1;

        if($model->save()){
            return 1;
        }

    }

    public function actionViewPendings(){

        $searchModel = new PaymentsSearch();
        $dataProvider = $searchModel->pending(Yii::$app->request->queryParams);

        return $this->render('view_pending', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionViewPayments($s_id,$o_id){
        return $this->render('payment_receive',['s_id' => $s_id,'o_id' => $o_id]);
    }

    public function actionGetPay($id){

        $model = $this->findModel($id);
        return $this->render('_form',['model' => $model]);
    }

    public function actionPayNow($id){

        $model = $this->findModel($id);

         $total = $_POST['total-paid'];
        
        if($total == $model->installment_amonut){
            $model->total_paid = $total;
            $model->status = 1;
            $model->mop = 1;
            $model->paid_by = Yii::$app->user->identity->getId();
            $model->payment_date =  date("Y-m-d H:i:s");
            $model->updated_by = Yii::$app->user->identity->getId();
            $model->updated_at = date("Y-m-d H:i:s");
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
                
            }
            else{
                /*echo '<pre>';
                echo print_r($model);
                echo '</pre>';
                exit;*/
            }
        }
        else{

            return 'Error!!!! you are not allowed to do this. please contact to Application vendor';

        }


    }


}
