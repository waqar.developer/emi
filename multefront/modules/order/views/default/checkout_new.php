<?php
use multebox\models\Cart;
use multebox\models\Inventory;
use multebox\models\File;
use multebox\models\Vendor;
use multebox\models\Address;
use multebox\models\Contact;
use multebox\models\search\MulteModel;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use multebox\models\Country;
use multebox\models\State;
use multebox\models\City;
use multebox\models\PaymentMethods;
?>

<script>
    $("#cart").hide();

    function Add_Error(obj,msg){
        $(obj).parents('.form-group').addClass('has-error');
        $(obj).parents('.form-group').append('<div style="color:#D16E6C; clear:both" class="error"><i class="icon-remove-sign"></i> '+msg+'</div>');
        return true;
    }

    function Add_Error2(obj,msg){
        $(obj).parents('.input-group').addClass('has-error');
        $(obj).parents('.input-group').append('<div style="color:#D16E6C; clear:both" class="error"><i class="icon-remove-sign"></i> '+msg+'</div>');
        return true;
    }

    function Remove_Error(obj){
        $(obj).parents('.form-group').removeClass('has-error');
        $(obj).parents('.form-group').children('.error').remove();
        return false;
    }

    function Remove_Error2(obj){
        $(obj).parents('.input-group').removeClass('has-error');
        $(obj).parents('.input-group').children('.error').remove();
        return false;
    }

    $(document).on("change", '.accountradio', function(event)
    {
        var accountval = $('input[name="account"]:checked').val();
        //alert(accountval);

        if(accountval == 'returning')
        {
            <?php
            Url::remember();
            ?>
            window.location.replace("<?=Url::to(['/site/login'])?>");
        }
    });

    $(document).on("click", '#button-coupon', function(event)
    {
        if ($('#input-coupon').val() != '')
        {
            Remove_Error2($('#input-coupon'));
            $.post("<?=Url::to(['/order/default/ajax-apply-discount'])?>", { 'discount_coupon': $('#input-coupon').val(), '_csrf' : '<?=Yii::$app->request->csrfToken?>'}) .done(function(result){
                if (result == 'a') // Invalid Coupon
                {
                    Add_Error2($('#input-coupon'), '<?=Yii::t('app','Coupon is Invalid!')?>');
                    event.preventDefault();
                }
                else if (result == 'b') // Expired Coupon
                {
                    Add_Error2($('#input-coupon'), '<?=Yii::t('app','Coupon has Expired!')?>');
                    event.preventDefault();
                }
                else if (result == 'c') // Not Applicable on Cart Items
                {
                    Add_Error2($('#input-coupon'), '<?=Yii::t('app','Coupon is not applicable on any of cart items!')?>');
                    event.preventDefault();
                }
                else if (result == 'd') // Not Applicable on Cart Amount
                {
                    Add_Error2($('#input-coupon'), '<?=Yii::t('app','Coupon is not applicable on current cart amount!')?>');
                    event.preventDefault();
                }
                else if (result == 'e') // Not issued to current user
                {
                    Add_Error2($('#input-coupon'), '<?=Yii::t('app','Coupon is not issued to you!')?>');
                    event.preventDefault();
                }
                else if (result == 'f') // Budget Exhausted
                {
                    Add_Error2($('#input-coupon'), '<?=Yii::t('app','Coupon already exhausted - Try another!')?>');
                    event.preventDefault();
                }
                else
                {
                    var coupontext = '<input type="hidden" name="coupon_code" value="'+$('#input-coupon').val()+'"><div class="panel-body"> <?=Yii::t('app', 'Coupon Applied Successfully')?>! <a href="javascript:void(0)" title="Remove" id="removecoupon" onClick=""><i class="fa fa-times-circle"></i></a></div>';

                    $('.discountcoupon').html(coupontext)

                    $('.cartcontents tbody').html(result);
                }
            })
        }
    });

    $(document).on("click", '#removecoupon', function(event)
    {
        var coupontext = '<label for="input-coupon" class="col-sm-3 control-label"><?=Yii::t('app', 'Enter coupon code')?></label>'+
            '<div class="input-group">'+
            '<div class="table-responsive">'+
            '<table>'+
            '<tr>'+
            '<td>'+
            '<input type="text" class="form-control" id="input-coupon" placeholder="<?=Yii::t('app', 'Enter your coupon here')?>" name="coupon">'+
            '</td>'+
            '<td>'+

            '<input type="button" class="btn btn-primary" data-loading-text="Loading..." id="button-coupon" value="<?=Yii::t('app', 'Apply Coupon')?>">'+

            '</td>'+
            '</tr>'+
            '</table>'+
            '</div>'+
            '</div>';
        $('.discountcoupon').html(coupontext);

        $.post("<?=Url::to(['/order/default/ajax-refresh-cartpage'])?>", {'_csrf' : '<?=Yii::$app->request->csrfToken?>'}) .done(function(result){
            $('.cartcontents tbody').html(result);
        })
    });

    $(document).on("click", '#button-confirm', function(event)
    {
        <?php
        Url::remember();
        ?>
        var guest = '<?=Yii::$app->user->isGuest?>';
        if (guest)
        {
            var error='';
            $('[data-validation="required"]').each(function(index, element)
            {
                Remove_Error($(this));

                var e=$(this).val();

                if($(this).val() == '' && !$(this).is("[mandatory-field]"))
                {
                    Remove_Error($(this));
                }
                else if($(this).val() == '' && $(this).is("[mandatory-field]"))
                {
                    error+=Add_Error($(this),'<?=Yii::t('app','This Field is Required!')?>');
                }
                else if($(this).is("[email-validation]"))
                {
                    var atpos=e.indexOf("@");
                    var dotpos=e.lastIndexOf(".");

                    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=e.length)
                    {
                        error+=Add_Error($(this),'<?=Yii::t('app','Email Address Not Valid!')?>');
                    }
                    else
                    {
                        Remove_Error($(this));
                    }
                }
                else if($(this).is("[num-validation]"))
                {
                    if (!e.match(/^\d+$/))
                    {
                        error+=Add_Error($(this),'<?=Yii::t('app','Please enter a valid number!')?>');
                    }
                    else
                    {
                        Remove_Error($(this));
                    }
                }
                else if($(this).is("[num-validation-float]"))
                {
                    //if (!e.match(/^\d+$/))
                    //if (!e.match(/^[-+]?[0-9]*\.?[0-9]+$/))
                    if (!e.match(/^[]?[0-9]*\.?[0-9]+$/))
                    {
                        error+=Add_Error($(this),'<?=Yii::t('app','Please enter a valid number!')?>');
                    }
                    else
                    {
                        Remove_Error($(this));
                    }
                }
                else if($(this).val() == '')
                {
                    error+=Add_Error($(this),'<?=Yii::t ('app','This Field is Required!')?>');
                }
                else
                {
                    Remove_Error($(this));
                }

                if(error !='')
                {
                    event.preventDefault();
                }
                else
                {
                    return true;
                }
            });
        }

        return true;
    });

    $(document).on("change", '#country_id', function(event){
        $.post("<?=Url::to(['/multeobjects/address/ajax-load-states'])?>", { 'country_id': $(this).val(), '_csrf' : '<?=Yii::$app->request->csrfToken?>'}) .done(function(result){
            $('#state_id').html(result);
            $('#city_id').html('<option value=""> --Select--</option>');
        })
    })

    $(document).on("change", '#state_id', function(event){
        $.post("<?=Url::to(['/multeobjects/address/ajax-load-cities'])?>", { 'state_id': $(this).val(), '_csrf' : '<?=Yii::$app->request->csrfToken?>'}) .done(function(result){
            $('#city_id').html(result);
        })
    })

</script>


<style id="compiled-css" type="text/css">
    .carousel-item {
        height: 100vh;
        min-height: 350px;
        background: no-repeat center center scroll;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    thead th {
        border: none !important;
    }

    /*for payment method*/
    .paymentWrap {
        /* padding: 50px; */
    }

    .paymentWrap .paymentBtnGroup {
        max-width: 900px;
        margin: auto;
        width: 470px;
    }

    .paymentWrap .paymentBtnGroup .paymentMethod {
        padding: 40px;
        box-shadow: none;
        position: relative;
    }

    .paymentWrap .paymentBtnGroup .paymentMethod.active {
        outline: none !important;
    }

    .paymentWrap .paymentBtnGroup .paymentMethod.active .method {
        border-color: #4cd264;
        outline: none !important;
        box-shadow: 0px 3px 22px 0px #7b7b7b;
    }

    .paymentWrap .paymentBtnGroup .paymentMethod .method {
        position: absolute;
        right: 3px;
        top: 3px;
        bottom: 3px;
        left: 3px;
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
        border: 2px solid transparent;
        transition: all 0.5s;
    }

    .paymentWrap .paymentBtnGroup .paymentMethod .method.visa {
        background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARwAAACxCAMAAAAh3/JWAAAAulBMVEX///8AYbL9uCcAXrFfjMRBe739tyH9vkH9wlQAUqwAVa4cabZciMLw9PkAWa8AX7EAWK+Xstba4/D9tAD/8dwzc7oAT6uOq9N2msufttn4+/2nvdxulMjo7vbU3+7/uxy2yOLH1ejD0ueCo89PgsAASqoucLixxOClvNzh6fPssTtKf7//vg59n81wl8mKiIOok3RwfpHKolp7goznrkJjeZeulm5Qc52ChYefkHkAQqf9yGmZjX32tS+aTgvuAAAKNElEQVR4nO2daXvaOBCAxYqkawOyYTfmvmmAprBts0f3+v9/ax2IQRrNDDbqNnmezPshX3y/1jEjyUQ1dU1ASRoql5NowecoJ7kREJKDnGigBJ/4KKf10vfxKhE5DCKHQeQwiBwGkcMgchhEDgOQs6+/eVZbSs69eel85sXJOpScu+SlE+EXJxY5NCKHQeQwiBwGkcMgchhEDoPIYRA5DCKHQeQwiBwGkcMgchhEDoPIYRA5DCKHQeQwiBwGkcMgchhEDoPIYRA5DCKHQeQwiBwGkcMgchhEDoPIYWDk6OStE5FyZNkbs+xNsBE5DCKHQeQwiBwGkcMgchhEDsOrl9OaLNabzWa96Hz/L5gryunURyfA1yZD4pBxE+xYbJg8NG0e5t6hk/YqjeI4fSKOs1p9uPX2+T+pKGf+VxbFaIqRjohDmqkbna+LDTt3w3tQMlpdHRltJ8KJTqNGd3zhFjddjOXhCYdL6hWiVK1Wrc6ifZMZP5nVD/gB88jd7e60xXnwmm6619lnBhshSEz2sOBucPveYLyfKDWodwbjOncw4Ko2p7XMvBvPz4PScPc8p3UT11o6s4/aRIj/QmPcmMGrnKnjv1oS55W2q5qz/aBb/jmvbJDHDe8eInTHTezsZM6Vb+k+fmbXqgf3KO9FxHcd/1IHOhl+yJP7paqvu/nf0lzbWw1qsOygv4cxiJ3dEmsn92i9Oh/UatDFptg96+H39UD83E26yRs5tVssvocctYWvKMJ6kr37mGn/tKXj1ipzaqdz72V+z8foCXI9UFet3fPa1GmvVbtbocO7Ps7ZgdcbIzcLCrndLnVBrTrfwV253zpKb5CbuqGONbt862S33GFKKa6XswUvyW1R0eeMrFDGbaitvm6ZlnJjMDdzquDUNBVpcAREyKAfMn1vj5nbrtrtCqhV6Sn+oFpU+LB33tVy7sn5AfvapQmQA7ob47d0GnTjVvzWpmoVUjESbQw4lb7H7mhGd3IJesAFAuQM3fLvF9wuYw/UqlMd8SuGiXSzt+zd20GhbqCJVoOeWEr0FU8YIGcBKg0MkcegNTbWNtBgmU2xAf7yXJItn7uXcTct3oauoW7WXHSUXfGEAXIm4F5giLxynzNeW9v6oFYVFW4AWpwktaO9dnQ4pdb47bITktf8LFmAnDGoASBEppMqBVvOc4MwA11V5Kbqg3qU5EUQv9sh281FlxJWhAA5AyjHPQmZVCmvxpl2sWHklja/T5prExOPCRJVUI5iKuFgCBnsSt3ruyFy332PxmmuN4Y4EoYHbeXRJJ6yDfqHG/dUsT9cdJEQOffM5ZmkSsH+2gqcQZNjKgy/gAtq0CRiMerlU14vB/QszuXppCqnRdWqFqip1CARQhdecAzkVBrmOhIipwfiGEsAk1Qpr+k81yoopxaXfSTQACYpPBdWQy8RIocJ8pikSjG1CvbkeTu+VqVY+iUVyNlVf8AQOXSIzCRVyq9V1ticPzQaEQM3LuCUecHJH426u9KEyIEh8jlPNnRSlbMGfZXV/SCDnKZRohPGmji3M78m8wyRA0PkWrEB1jcwbAtC55q1aY0EcnkGcelOYKbyVHBA1b4m8wyRQ4XIXFKlvIbFzeZjLAcwCTvh4BW4Y9/odqZJUv0Bg2Y8iRAZJlUz9yiQIkTO2FwbzwGiOpcabbGCo/agU6j+fEFyQNPy3HiwSZXy3igYSyCGHTRUbLPCCg7swK7IPIPk3KEhMpdUKS8igWNk3sB9ITEip+MmaMGB42lXZJ5BcmCIfIhJYFK1BweBWuWNyy+ocVKTEBMHN2jBgZHGFZlnkBwsRB5Ebo4Tw9LsNp6wtc5ZU3aSbIbdxdwLjo+ASOOKzDNIThsJkUdcUvXE5cB14U82P5Nt/L1hNH664jw48wySA6I5Xb+UVKly77OjqeknxM6CKDiqE5x5BslBQmQ+qVJwOCuJ0RMPyMnyzIt4QPt/LqrhmWeQHPBu8jhrzSZVT4B+HjbXBZsMLzwJvFWQxp0LDh9sliJIjh8iMzNVR2CtIkPf8R1eeKDvGlVwvkHmGbYmEPQres8nVcoLW/FadaSPFx63pg7JgvMNMs8wOaCggOUR2ESamzvxb7N1gxUe9yE1XXBAjJqgE8gsYXL4b9eQiH9etlYd2WCdurUgA0acTsG5kKeUIEwO+5+xsLl+sG7lYjK4RdYx2c5TpuDAeZ7qmWeYHLhGx33DSLzupqq6xOrFB+8SVksGBo5AuB2ceYbJaTNyvKRKecNjpYJWbyHbufsHqcpTKDM4E555hslZ03L8pErxiyRJvFb/1Iov4eVNZAOXnlXOPMPkzOl1DTGWBrFLjynA9GjNFEPurXLrnE53VDnzDJPTIeWgy5Lh0mN32sVfGXZkCy5yanN6XIvnUz3zDJPjTcKd8JMqdaFWLd7X8MWMUE7RW42rFZwrMs/Ar2ao+8NrDNjHieYGaZJke6wRgktLijhnxMURCNUzz0A5CREFZtgJ6aXH6nkc2MT+A7TASHURPlHjqbScyplnoBxi3S+SVClu6fEpR0rStOveywR2VkXtqPyvWatnnoFy8M8wiEidWiSp7PYjMdlqXUQkrdkDbNWKEaCSS3LtC1bOPAPl4CEyPo2ypZYe59zbkrWJ9Gq/269qUerJL1oOGBtq5DsioLVy5hkoBw2R8QXU9NLjvMLBqbxEa6NhhTpseJ48hh856FXPY3fvHlp5zjNQDja3XcvwKRR3IZhtcFK+imTP3T0YjnXT8YJR+cEjlEA5WIhs8EUjcOmxFfNRfZ5P/NzSw7XcKdpNh2aegXKQEBlNqhS99NibzWEwRfgEliPiBSc48wyUg4TIaFKlvHG584qQ+V+l3RRLBOFHDnjBCZ7zDP2u3GstqG89yaXHeeKg0W9dPeJT2N0oVXC8meeqmWeoHK+5QJMqxSw9fqIdpxf1WGuY4EcORMEJnvMMlQNDZHIYglwkeaSvYz7iNeZsHX48QhSc4DnPUDn1GHy/TZxp7H7uHfnvelHPUiyyOarJrDn1fmYunazsRXlC5Sz6LjNiv0m3bYP+rsBgNjJR6rc/Oo169v5tQJfqogfuRdsX5jo8XtsPfWyHvfsoitP8RR9+xMGkcbYafv/f+Djw2uQcGM9n/W5vVG82R8thlc95vzGvUs5rQeQwiBwGkcMgchhEDoPIYRA5DEDO3z++ed79TMl5d/vm+YmR88NbR+QwiBwGkcMgchhEDoPIYRA5DCKHQeQwiBwGkcMgchhEDoPIYRA5DCKHQeQwiBwGkcMgchhEDoPIYRA5DCKHQeRg3E5zRA7K7a+Pvzx+mYocjA+fv3z69c8vMleOMP1l+jj94+PjVOT4TL9+/Pz508evIgdh+ueHx+lnKTk4099+//jljw/S5qBMP339599btrd6w+Rhzi23suvHd2+eH0g5go3IYRA5DCKHQeQwiBwGkcMgchie5bzQt8mvnKMc3awLPvogp6YFhNpRjoCSNP4DOMmKSOf115wAAAAASUVORK5CYII=");
    }

    .paymentWrap .paymentBtnGroup .paymentMethod .method.master-card {
        background-image: url("<?= Yii::$app->homeUrl?>/img/cod.jpeg");
    }

    .paymentWrap .paymentBtnGroup .paymentMethod .method.amex {
        background-image: url("<?= Yii::$app->homeUrl?>/img/shopping-cart-bitmap@2x.png");
    }

    .paymentWrap .paymentBtnGroup .paymentMethod .method.vishwa {
        background-image: url("http://i.imgur.com/VkiM7PL.jpg");
    }

    .paymentWrap .paymentBtnGroup .paymentMethod .method.ez-cash {
        background-image: url("http://www.busbooking.lk/img/carousel/BusBooking.lk_ezCash_offer.png");
    }


    .paymentWrap .paymentBtnGroup .paymentMethod .method:hover {
        border-color: #4cd264;
        outline: none !important;
    }
</style>

<?php
/**
 * setup merchant id and secured key
 */
$merchantid = "204";
$secret = "bTO3NfZJaHTYXL8c";
/**
 * token URL
 */
$token_url = "https://ipg1.apps.net.pk/Ecommerce/api/Transaction/GetAccessToken?MERCHANT_ID=".$merchantid."&SECURED_KEY=".$secret;
$contents = file_get_contents($token_url);

/**
 * decode JSON returned data
 */
$token_info = json_decode($contents);
$token = "";
if (isset($token_info->ACCESS_TOKEN)) {
    $token = $token_info->ACCESS_TOKEN;

}
/**
 * initialize form with required parameters
 */
?>

<section class="py-5 text-center">

    <form method="post" id="checkoutform" action="https://ipg1.apps.net.pk/Ecommerce/api/Transaction/PostTransaction">
        <INPUT TYPE="hidden" NAME="MERCHANT_ID" target="payfast" VALUE="<?=$merchantid; ?>">
        <INPUT TYPE="hidden" NAME="MERCHANT_NAME" value="KistPay">
        <INPUT TYPE="hidden" NAME="TOKEN" VALUE="<?=$token?>">
        <INPUT TYPE="hidden" NAME="PROCCODE" VALUE="00">

        <INPUT TYPE="hidden" NAME="SIGNATURE" VALUE="RANDOMSjjhjTRINGVALUE">
        <INPUT TYPE="hidden" NAME="VERSION" VALUE="MY_VER_1.0">

        <INPUT TYPE="hidden" NAME="SUCCESS_URL" VALUE="http://kistpay.com/multefront/web/order/default/create-vochers?">
        <INPUT TYPE="hidden" NAME="FAILURE_URL" VALUE="http://kistpay.com/multefront/web/site/index?">

        <INPUT TYPE="hidden" NAME="ORDER_DATE" VALUE="<?=date('Y-m-d')?>">
        <INPUT TYPE="hidden" NAME="CHECKOUT_URL" VALUE="http://merchantsite.com/order/backend/confirm">

        <div class="container mt-6">
            <div class="row">
                <?php
                if(count($cart_items) == 0)
                {
                    ?>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td><h1 class="title font"><?=Yii::t('app', 'Your cart is empty - Why not take a look at huge number of wonderful products available!')?></h1></td>
                            </tr>
                            </thead>
                        </table>
                    </div>

                    <?php
                }
                else
                {
                    ?>

                    <div class="col-md-12 col-sm-12 mt-5">
                        <h2 class="cart-heading mb-5 text-left font"><?=Yii::t('app','Your Order is Here')?></h2>
                        <table class="table table-hover font">
                            <thead>
                            <tr>
                                <th><?=Yii::t('app','Product')?></th>
                                <th><?=Yii::t('app','Quantity')?></th>
                                <th><?=Yii::t('app','Payment Plan')?></th>
                                <th class="text-center"><?=Yii::t('app','Price')?></th>

                                <th class="text-center"><?= ($cart_items->plan_id != 0)? Yii::t('app','Payable') :Yii::t('app','Payable')?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $total_cart_price = 0;
                              $cart= $cart_items;

                                $inventory_item = Inventory::findOne($cart['inventory_id']);


                                $prod_title = $inventory_item->product_name;

                                $fileDetails = File::find()->where("entity_type='product' and entity_id=".$inventory_item->product_id)->one();
                                ?>
                                <tr class="bg-light-color">
                                    <td class="col-sm-8 col-md-6">
                                        <div class="media">
                                            <a class="thumbnail pull-left" href="#"> <div class="cart-product-container"><img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" class="media-object"></div> </a>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href="#"><?=$prod_title?></a></h4>
                                                <span>Status: </span><span class="text-success"><strong><?=Yii::t('app','In Stock')?></strong></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-sm-1 col-md-1" style="text-align: center">
                                        <input type="hidden" name="special_discount" value="0">
                                        <input type="email" class="form-control" id="exampleInputEmail1" value="<?=$cart->total_items?>" readonly>
                                    </td>

                                    <td class="col-sm-1 col-md-1 text-center"><strong><?php if($cart->plan_id != 0) {echo MulteModel::getplan($cart->plan_id);}else{echo 'Full Cash';}?></strong></td>
                                    <td class="col-sm-1 col-md-1 text-center"><strong><?=MulteModel::formatAmount(MulteModel::getInventoryActualPrice($inventory_item) - MulteModel::getInventoryDiscountAmount($inventory_item, $cart->total_items))?></strong></td>
                                    <?php
                                    $total_cart_price += MulteModel::getInventoryTotalAmount($inventory_item, $cart->total_items)*$cart->total_items;
                                    ?>

                                    <td class="col-sm-1 col-md-1 text-center"><strong><?php echo ($cart->plan_id != 0) ?   round(MulteModel::getAdvance($total_cart_price,$cart->plan_id)): MulteModel::formatAmount(MulteModel::getInventoryActualPrice($inventory_item) - MulteModel::getInventoryDiscountAmount($inventory_item, $cart->total_items))?></strong></td>


                                </tr>


                            </tbody>
                            <tfoot>


                            <tr>
                                <td colspan="4">
                                    <h5 class="text-right"><?=Yii::t('app','Subtotal')?></h5>

                                </td>
                                <td class="text-right">
                                    <h5><strong><?php echo ($cart->plan_id != 0) ?   round(MulteModel::getAdvance($total_cart_price,$cart->plan_id)): MulteModel::formatAmount(MulteModel::getInventoryActualPrice($inventory_item) - MulteModel::getInventoryDiscountAmount($inventory_item, $cart->total_items))?></strong></h5>

                                </td>
                            </tr>

                            <?php
                            $email = \Yii::$app->user->identity->email;
                            $mobile = \Yii::$app->user->identity->mobile;
                            ?>

                            <INPUT TYPE="hidden" NAME="TXNAMT" VALUE ="<?php echo ($cart->plan_id != 0) ?   round(MulteModel::getAdvance($total_cart_price,$cart->plan_id)): $total_cart_price?>">
                            <INPUT TYPE="hidden" NAME="BASKET_ID" VALUE="<?=$cart->id?>">
                            <INPUT TYPE="hidden" NAME="TXNDESC" VALUE="<?=$inventory_item->product_name?>">
                            <INPUT TYPE="hidden" NAME="CUSTOMER_MOBILE_NO" VALUE="<?=$mobile?>">
                            <INPUT TYPE="hidden" NAME="CUSTOMER_EMAIL_ADDRESS" VALUE="<?=$email?>">
                            <tr>
                                <td colspan="5">
                                    <div class="paymentCont">


                                        <div class="footerNavWrap clearfix">


                                            <button type="submit" class="btn btn-success pull-right btn-fyi font"><?=Yii::t('app', 'Proceed to Payment')?></button><span class="glyphicon glyphicon-chevron-right"></span>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                <?php } ?>
            </div>
        </div>
    </form>
</section>


