<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerDocuments */

$this->title = 'Create Customer Documents';
$this->params['breadcrumbs'][] = ['label' => 'Customer Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-documents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
