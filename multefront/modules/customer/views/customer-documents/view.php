<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\helpers\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */



?>
<style>

    .title{
        text-align: center;padding: 15px;font-weight: bold;text-decoration: underline;letter-spacing: 2px;font-size: 25px;
    }
</style>
<div class="customer-view">

<?php ?>
</div>

        <?php
        $id = Yii::$app->user->identity->id;
            // $query = \multebox\models\CustomerDocuments::find()->where(['=','customer_id',$id])->all();
        ?>
<div class="wrapper wrapper-content animated fadeInRight">





    <div class="row">
        <div class="col-lg-11">



                            <div class="row">
                                <?php  ?>

                                    <div class="col-lg-6">
                                        <div class="ibox ">
                                            <div class=" text-center p-md">

                                                <h4 class="m-b-xxs"><?='NTN #'?></h4>
                                                <div class="m-t-md">

                                                    <div class="p-lg ">
                                                        <embed src="<?=app\helpers\Helper::getBaseUrl()?>drive/customerDocuments/CNIC_front-20190423054027.jpg"
                                                               width="350px" height="200px"/>
                                                    </div>
                                                    <a href="<?=Helper::getBaseUrl()?>drive/customerDocuments/CNIC_front-20190423054027.jpg"><button type="button" class=" btn btn-success">
                                                        Download
                                                    </button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-lg-6">
                                    <div class="ibox ">
                                        <div class=" text-center p-md">

                                            <h4 class="m-b-xxs"><?='CNIC Front'?></h4>
                                            <div class="m-t-md">

                                                <div class="p-lg ">
                                                    <embed src="<?=app\helpers\Helper::getBaseUrl()?>drive/customerDocuments/Utility_Bill-20190424032205.jpg"
                                                           width="350px" height="200px"/>
                                                </div>
                                                <a href="<?=Helper::getBaseUrl()?>drive/customerDocuments/Utility_Bill-20190424032205.jpg"><button type="button" class=" btn btn-success">
                                                        Download
                                                    </button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="ibox ">
                                        <div class=" text-center p-md">

                                            <h4 class="m-b-xxs"><?='Utility Bill'?></h4>
                                            <div class="m-t-md">

                                                <div class="p-lg ">
                                                    <embed src="<?=app\helpers\Helper::getBaseUrl()?>drive/customerDocuments/NTN_Number-20190423054027.jpg"
                                                           width="350px" height="200px"/>
                                                </div>
                                                <a href="<?=Helper::getBaseUrl()?>drive/customerDocuments/NTN_Number-20190423054027.jpg"><button type="button" class=" btn btn-success">
                                                        Download
                                                    </button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="ibox ">
                                        <div class=" text-center p-md">

                                            <h4 class="m-b-xxs"><?='CNIC Back'?></h4>
                                            <div class="m-t-md">

                                                <div class="p-lg ">
                                                    <embed src="<?=app\helpers\Helper::getBaseUrl()?>drive/customerDocuments/Utility_Bill-20190424032205.jpg"
                                                           width="350px" height="200px"/>
                                                </div>
                                                <a href="<?=Helper::getBaseUrl()?>drive/customerDocuments/Utility_Bill-20190424032205.jpg"><button type="button" class=" btn btn-success">
                                                        Download
                                                    </button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <?php
                                ?>
                            </div>


        </div>
