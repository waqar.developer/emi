
var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+"/multefront/web/";
function sendOTP() {

    $(".error").html("").hide();
    var number = $("#mobile").val();
    var email = $("#email").val();

    var url = baseUrl+'site/verification';

    if ( number.length == 11 && number != null) {

        startTimer();
    }
    else{
        alert('Please enter a valid number!');
        die();
    }


	if ( number.length == 11 && number != null) {
		var input = {
			"mobile_number" : number,
            "email" : email,
			"action" : "send_otp"
		};
		$.ajax({
			url : url,
			type : 'GET',
			data : input,
			success : function(response) {
			    if('yes'){
                   // alert('6 digits code is sent to your mobile. Please check you mobile');





                }
            }
		});
	} else {
		//alert('Please enter a valid number!');
		$(".error").show();
	}
}

function verifyOTP() {
	$(".error").html("").hide();
	$(".success").html("").hide();
	var otp = $("#mobileOtp").val();
    var url = baseUrl+'site/verification';

	var input = {
		"otp" : otp,
		"action" : "verify_otp"
	};
	if (otp.length == 6 && otp != null) {
		$.ajax({
			url : url,
			type : 'POST',
			data : input,
			success : function(response) {
                if(response == 'yes'){
                    alert('Your Mobile number is verified');
                    $("#mobile").attr("readonly", "true");
                    $("#mobverify").attr("disabled", "disabled");
                    $("#mobileOtp").attr("readonly", "true");
                    $("#mobsend").attr("disabled", "disabled");
                    $('input[name="SignupForm[mobotpcheck]"]').val(1);
                    $('#before-confirm').hide();
                    $('#button-confirm').show();
                    $('#beforesubmit').hide();
                    $('#signsubmit').show();
                    $('#error').empty();

                }
                else{
                    alert('you entered wrong code');
                }
				$("." + response.type).html(response.message);
				$("." + response.type).show();
			},
			error : function() {

				alert('error');
			}
		});
	} else {
        alert('you entered wrong otp');

	}
}

function sendmail() {

    $(".error").html("").hide();
    var email = $("#email").val();
    var e_patt = new RegExp(/^[a-z0-9_-]+(\.[a-z0-9_-]+)*@[a-z0-9_-]+(\.[a-z0-9_-]+)*(\.[a-z]{2,4})$/);
    var url = baseUrl+'site/email';


    if (email != null && e_patt.test(email)) {
        var input = {
            "email" : email,
            "action" : "send_email"
        };
        $.ajax({
            url : url,
            type : 'POST',
            data : input,
            success : function(response) {

                alert('6 digits code sent to you email please check your Email.');

            }

        });
    } else {
        alert('Please enter a valid Email!');
        $(".error").hide();


    }
}

function verifymail() {
    $(".error").html("").hide();
    $(".success").html("").hide();
    var otp = $("#emailotp").val();
    var url = baseUrl+'site/email';

    var input = {
        "otp" : otp,
        "action" : "verify_email"
    };
    if (otp.length == 6 && otp != null) {
        $.ajax({
            url : url,
            type : 'POST',
            data : input,
            success : function(response) {

                if(response == 'yes'){
                    alert('Your Email is verified');
                    $("#email").attr("readonly", "true");
                    $("#emailotp").attr("readonly", "true");
                    $("#esend").attr("disabled", "disabled");
                    $("#everify").attr("disabled", "disabled");
                    $('input[name="SignupForm[eotpcheck]"]').val(1);
                }
                else{
                    alert('Please Enter valid 6 digit code');
                }
                $("." + response.type).html(response.message)
                $("." + response.type).show();
            },
            error : function() {
                alert('error');
            }
        });
    } else {
        alert('you entered wrong otp');

    }
}