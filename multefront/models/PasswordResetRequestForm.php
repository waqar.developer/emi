<?php
namespace multefront\models;

use Yii;
use yii\base\Model;
use multebox\models\User;
use multebox\models\SendEmail;
use yii\helpers\Html;
use app\helpers\Helper;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $mobile;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            [['email','mobile'], 'required','message'=> Yii::t('app', "You can't leave this empty.")],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\multebox\models\User',
                'filter' => ['active' => User::STATUS_ACTIVE],
                'message' => Yii::t('app', 'There is no user with this email address.')
            ],
        ];
    }

	public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'mobile' => Yii::t('app', 'Mobile #'),
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'active' => User::STATUS_ACTIVE,
            'email' => $this->email,
            'mobile'=>$this->mobile,
        ]);
        /*echo '<pre>';
        echo print_r($user);
        echo '</pre>';exit;*/

        if (!$user) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();

            if (!$user->save()) {
                return false;
                exit;
            }

        }

		$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['/site/reset-password', 'token' => $user->password_reset_token]);
        $emailsubject = 'Password Reset Link';
        $text = "<img src='http://kistpay.maaliksoft.com/emi/multefront/web//img/homepage-group-9-1@2x.png' class='img-slider'><br>Dear Customer! <br> You recently made a request to reset your password. Please click the link below to continue.<br> ".$resetLink." <br>  If you did not make this change or you believe an unauthorized person has accessed your account, kindly email us immediately on info@kistpay.com.<br>Sincerely,<br>Team Kistpay";

         $email = Helper::sendEmail($this->email,$text,$emailsubject);

         return true;


		//return !SendEmail::sendResetRequestEmail($resetLink, $this->email, $user->first_name, $user->last_name);
    }
}

