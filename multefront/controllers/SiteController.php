<?php
namespace multefront\controllers;

use Yii;
use multebox\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use multebox\models\LoginForm;
use multebox\models\Cart;
use multebox\models\Vendor;
use multebox\models\ImageUpload;
use multebox\models\SendEmail;
use yii\base\UserException;
use multebox\models\SignupForm;
use multefront\models\PasswordResetRequestForm;
use multefront\models\ResetPasswordForm;
use multebox\models\AddressModel;
use multebox\models\ContactModel;
use multebox\models\Address;
use multebox\models\Contact;
use multebox\models\AuthAssignment;
use multebox\models\User;
use multebox\models\search\UserType as UserTypeSearch;
use multefront\controllers\Textlocal;
use app\helpers\Helper;
require ('textlocal.class.php');

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'signup'],
                        'allow' => true,
						'roles' => ['?'],
                    ],
					[
                        'actions' => ['error', 'index'],
                        'allow' => true,
						'roles' => ['?', '@'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $lang = $_REQUEST['lang'];
		\Yii::$app->language = $lang;
        $this->layout = 'main_new';
        return $this->render('homepage');
    }

    public function actionIndexnew()
    {
        $this->layout = 'main_new';
        $lang = $_REQUEST['lang'];
        \Yii::$app->language = $lang;
        $this->layout = 'main_new';
        return $this->render('homepage');
    }

    public function actionInfo()
    {
        $this->layout = 'main_new';
        $lang = $_REQUEST['ur-UR'];
        \Yii::$app->language = $lang;
        $this->layout = 'main_new';
        $order = \multebox\models\SubOrder::find()->where(['=','id',$_REQUEST['order_id']])->one();
        return $this->render('information',['order'=>$order]);
    }



    /**
     * Login action.
     *
     * @return string
     */

    public function actionLogin()
    {

		//print_r(Yii::$app->user->identity->id);exit;
        if (!Yii::$app->user->isGuest) {
            $lang = $_REQUEST['lang'];
            \Yii::$app->language = $lang;
            $this->layout = 'main_new';
            return $this->render('homepage');
        }

		$old_session_id = session_id();
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			
			if (Yii::$app->user->identity->entity_type != 'customer')
			{
				Yii::$app->user->logout();
				throw new UserException('You are not an authenticated user of frontend system!');
			}
			else
			{
				$old_cart = Cart::find()->where("session_id = '".$old_session_id."'")->all();
				foreach($old_cart as $cart_item)
				{
					$currentcart = Cart::find()->where("user_id = ".Yii::$app->user->identity->id." and inventory_id = ".$cart_item->inventory_id)->one();

					if(count($currentcart) > 0)
					{
						$currentcart->total_items += $cart_item->total_items;
						$currentcart->updated_at = time();
						$currentcart->save();
						Cart::findOne($cart_item->id)->delete();
					}
					else
					{
						$cart_item->session_id = session_id();
						$cart_item->user_id = Yii::$app->user->identity->id;
						$cart_item->updated_at = time();
						$cart_item->save();
					}
				}
                $lang = $_REQUEST['lang'];
                \Yii::$app->language = $lang;
                $this->layout = 'main_new';
                return $this->render('homepage');
			}
        } else {
            $lang = $_REQUEST['lang'];
            Yii::$app->language = $lang;
            $this->layout = 'main_new';
            return $this->render('login_new', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        $this->layout = 'main_new';
        return $this->render('homepage');
    }

	public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) 
		{

            if ($user = $model->signup()) 
			{
                if (Yii::$app->getUser()->login($user))
				{
                    return $this->goHome();
                }
            }

        }
        $lang = $_REQUEST['lang'];
        Yii::$app->language = $lang;
        $this->layout = 'main_new';
        return $this->render('signup_new', [
            'model' => $model,
        ]);
    }

	public function actionVendorSignup()
    {
        $model = new Vendor;
		$img = new ImageUpload();
		$emailObj = new SendEmail;
		$connection = Yii::$app->db;

		try
		{
			$transaction = $connection->beginTransaction();

			if ($model->load(Yii::$app->request->post()) && $model->save()) 
			{
				$address_id = AddressModel::addressInsert($model->id,'vendor');
				
				$model->added_at = strtotime(date('Y-m-d H:i:s'));
				$model->update();

				//Vendor Add Contact
				$contact_id = ContactModel::contactInsert($model->id,'vendor', $address_id, true); //primary
				$contact = Contact::findOne($contact_id);

				//Create Vendor User to Login to Backend
				if(User::find()->where("email='".$contact->email."'")->count() > 0)
				{
					 return $this->redirect(['view', 'id' => $model->id,'error'=>Yii::t('app', 'User can not be Created Email Already Exists!')]);
				}
				else
				{
					$userModel = new User;
					$userModel->first_name = $contact->first_name;
					$userModel->last_name = $contact->last_name;
					$userModel->email = $contact->email;
					$userModel->username = $contact->email;
					$userModel->active = 0;
					$userModel->user_type_id = UserTypeSearch::getCompanyUserType('Vendor')->id;
					$userModel->entity_id = $model->id;
					$userModel->entity_type = 'vendor';
					$userModel->added_at = time();
					$new_password = Yii::$app->security->generateRandomString (8);
					$userModel->password_hash=Yii::$app->security->generatePasswordHash($new_password);
					$userModel->save();
					/*if(count($userModel->errors) >0){
						var_dump($userModel->errors);
					}*/
					$authModel = new AuthAssignment;
					$authModel->item_name = 'Vendor';
					$authModel->user_id = $userModel->id;
					$authModel->save();
					$img->loadImage('../../multeback/web/users/nophoto.jpg')->saveImage("../../multeback/web/users/".$userModel->id.".png");
					$img->loadImage('../../multeback/web/users/nophoto.jpg')->resize(30, 30)->saveImage("../../multeback/web/users/user_".$userModel->id.".png");
					SendEmail::sendNewUserEmail($userModel->email,$userModel->first_name." ".$userModel->last_name, $userModel->username,$new_password);
				}
				
				Yii::$app->session->setFlash('success', Yii::t('app', 'Registration completed - awaiting admin approval!'));

				$transaction->commit();
				return $this->redirect(['/site/index']);
			} 
			else 
			{
				
				return $this->render('vendor-signup', [
					'model' => $model,
				]);
			}
		}
		catch (\Exception $e)
		{
			$transaction->rollback();
			Yii::$app->session->setFlash('error', $e->getMessage());
			return $this->render('vendor-signup', [
					'model' => $model,
				]);
		}
    }

	/**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {

                $this->layout = 'main_new';
                Yii::$app->language = $_REQUEST['lang'];
                return $this->render('requestPasswordResetToken', [
                    'model' => $model,
                    'success' =>'1',
                ]);exit;

            } else {
                Yii::$app->language = $_REQUEST['lang'];
                $this->layout = 'main_new';
                return $this->render('requestPasswordResetToken', [
                    'model' => $model,
                    'success' =>'2',
                ]);exit;
            }
        }
        Yii::$app->language = $_REQUEST['lang'];
        $this->layout = 'main_new';
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

	/**
     * Resets password.
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'New password saved.'));
            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionVerification()
    {
        switch ($_REQUEST["action"]) {
            case "send_otp":
                $mobile_number = $_REQUEST['mobile_number'];
                $mobile_number = $mobile_number*1;
                 $mobile_number = '92'.$mobile_number;



                $numbers = array(
                    $mobile_number
                );
                $sender = 'EMI';
                $otp = rand(100000, 999999);
                echo $otp;
                $_SESSION['session_otp'] = $otp;
                $message ="Your Kistpay account registration code is: ".$otp."\nPlease use above code to register your account with Kistpay \nIncase of any queries, please email us on info@kistpay.com \nRegards,\nTeam Kistpay";
                $sms = Helper::sendSMS($mobile_number,$message);
                $emailsubject = 'KistPay Email verification';
                $email =  $_REQUEST['email'];
                $emailmessage = "<img src='http://kistpay.maaliksoft.com/emi/multefront/web//img/homepage-group-9-1@2x.png' class='img-slider'><br>Dear Customer! <br> Your Kistpay account registration code is: ".$otp." <br> Please use above code to register your account with Kistpay <br> Incase of any queries, please email us on info@kistpay.com <br>Regards,<br>Team Kistpay";
                $email = Helper::sendEmail($email,$emailmessage,$emailsubject);

                if($sms){
                    echo 'yes';
                }else{
                    echo  'no';
                }

                break;

            case "verify_otp":
                $otp = $_REQUEST['otp'];

                if ($otp == $_SESSION['session_otp']) {
                    unset($_SESSION['session_otp']);
                    echo 'yes';
                } else {
                    return 'no';
                }

                break;
        }
    }


    public function actionEmail(){

        switch ($_REQUEST["action"]) {
            case "send_email":
                $email =  $_REQUEST['email'];
                $emailsubject = 'KistPay Email verification';
                $otp = rand(100000, 999999);
                $_SESSION['session_otp'] = $otp;
                $emailmessage = "Your Kistpay account registration code is: ".$otp."\nPlease use above code to register your account with Kistpay \nIncase of any queries, please email us on info@kistpay.com \nRegards,\nTeam Kistpay";
                $email = Helper::sendEmail($email,$emailmessage,$emailsubject);
                if($email){
                    echo 'yes';

                }else{
                    echo 'no';
                }
                break;

            case "verify_email":
                $otp = $_REQUEST['otp'];

                if ($otp == $_SESSION['session_otp']) {
                    unset($_SESSION['session_otp']);
                    echo 'yes';
                } else {
                    return 'no';
                }
                break;
        }
    }

    public function actionSendemail(){

        $email = $_REQUEST['e'];
        $emailsubject = 'teting purpose new';
        $emailmessage = $_REQUEST['m'];
        $email = Helper::sendEmail($email,$emailmessage,$emailsubject);
        if($email){
            echo 'yes';

        }else{
            echo 'no';
        }
    }
    public function actionSendsms(){

        $phone = $_REQUEST['n'];
        $message = 'teting purpose new';

        $email = Helper::sendSMS($phone,$message);
        if($email){
            echo 'yes';

        }else{
            echo 'no';
        }
    }

    public function actionResetpwd(){

        $phone = $_REQUEST['mob'];
        $email = $_REQUEST['email'];
        $user = User::find()->where(['=','email',$email])->andWhere(['=','mobile',$phone])->one();
      echo  $user->password_hash;
        $pwd = Yii::$app->getSecurity()->decryptByPassword($user->password_hash,'QzizAmep1jB65wsvpI-6lpRIZfQSAAXV');

        echo $pwd;exit;$message = 'teting purpose new';

        $email = Helper::sendSMS($phone,$message);
        if($email){
            echo 'yes';

        }else{
            echo 'no';
        }
    }

    public function actionTerms(){

        $lang = $_REQUEST['lang'];
        \Yii::$app->language = $lang;
        $this->layout = 'main_new';
        return $this->render('terms-conditions');
    }
}
