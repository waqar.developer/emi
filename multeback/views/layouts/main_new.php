<?php

/* @var $this \yii\web\View */
/* @var $content string */
namespace app\components;
use yii;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use multeback\assets\AppAsset;

AppAsset::register($this);



function activeParentMenu($array)
{
	return in_array(Yii::$app->controller->route,$array)?'active':'';	
}

function activeMenu($link)
{
	return Yii::$app->controller->route==$link?'active':'';	
}

function activeEstimateMenu($entity_type)
{
	return ($_REQUEST['entity_type'] == $entity_type ) ? 'active' : '';
}

function activeSubMenu($action, $entity_type)
{
	$path = parse_url( $_SERVER['REQUEST_URI']);
	$route = Yii::$app->controller->route;
	$route = explode( "/", trim( $route, "/" ) );
	return ( $action == $route[2] && $_REQUEST['entity_type'] == $entity_type ) ? 'active' : '';
}


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <script src="<?= Yii::$app->homeUrl?>theme/js/jquery-3.1.1.min.js"></script>
    <style>
        .text-muted {
            color: #1bb394 !important;
        }
    </style>
</head>
<body>
	
<?php $this->beginBody() ?>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">

                        <img style="width: 40px; height: 40px;" alt="image" class="rounded-circle" src="files/profile_image/"/>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
						
                            <span class="block m-t-xs font-bold"></span>
                            <span class="text-muted text-xs block">Art Director <b class="caret"></b></span>
                        </a>
						
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a class="dropdown-item" href="<?=Yii::$app->homeUrl?>user/profile">Profile</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="<?=Yii::$app->homeUrl?>site/logout">Logout</a></li>
                        </ul>
                    </div>
					
                    <div class="logo-element">
                        IN+
                    </div>
                </li>
				
                <li>
                    <a href="<?=Yii::$app->homeUrl ?>site/index"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
                </li>
				
                <?php if(Yii::$app->user->can('products/index')){?>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Products</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?=Yii::$app->homeUrl ?>products/create">Add New</a></li>
                        <li><a href="<?=Yii::$app->homeUrl?>products/index">View All</a></li>

                    </ul>
                </li>
                <?php } if(Yii::$app->user->can('category/index')){ ?>
                <li>
                    <a href="<?=Yii::$app->homeUrl ?>category"><i class="fa fa-th-large"></i> <span class="nav-label">Category</span></a>
                </li>
                <?php }  if(Yii::$app->user->can('banks/index')){?>
                <li>
                    <a href="#"><i class="fa fa-desktop"></i> <span class="nav-label">Banks</span>  <span class="float-right label label-primary">16/24</span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?=Yii::$app->homeUrl?>banks/create">Add Bank</a></li>
                        <li><a href="<?=Yii::$app->homeUrl?>banks/index">All Banks</a></li>
                    </ul>
                </li>
			
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Insurance Company</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="<?=Yii::$app->homeUrl ?>insurance/create">Add New</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>insurance/index">View All</a></li>

                        </ul>
                    </li>
                <?php }  if(Yii::$app->user->can('orders/index')){ ?>
                <li>
                    <a href="#"><i class="fa fa-desktop"></i> <span class="nav-label">Orders</span>  <span class="float-right label label-primary">16/24</span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?=Yii::$app->homeUrl?>orders/create">Make Order</a></li>
                        <li><a href="<?=Yii::$app->homeUrl?>orders/index">All Orders</a></li>
                    </ul>
                </li>
                <?php }  if(Yii::$app->user->can('customers/index')){ ?>
                <li>
                    <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Customers</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?= Yii::$app->homeUrl?>customer/index">Customer Detail</a></li>
                        <li><a href="<?= Yii::$app->homeUrl?>customer/create">Add New Customer</a></li>
                    </ul>
                </li>
                <?php }  if(Yii::$app->user->can('products/index')){?>
                <li>
                    <a href="#"><i class="fa fa-calculator"></i> <span class="nav-label">Payment Plans</span><span class="label label-info float-right">NEW</span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?= Yii::$app->homeUrl?>payment-plan-template/create">Add New</a></li>
                        <li><a href="<?= Yii::$app->homeUrl?>payment-plan-template/index">View All Plans</a></li>

                    </ul>
                </li>
                <?php }?>
                <li>
                    <a href="#"><i class="fa fa-user-circle"></i> <span class="nav-label">Payments</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?=Yii::$app->homeUrl?>payments/index">View Payment</a></li>
                    </ul>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?=Yii::$app->homeUrl?>payments/view-pendings">Pending Payments</a></li>
                    </ul>
                </li>
                <?php  if(Yii::$app->user->can('users/index')){?>
                <li>
                    <a href="#"><i class="fa fa-user-circle"></i> <span class="nav-label">Users</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?=Yii::$app->homeUrl?>user/create">Add User</a></li>
                    </ul>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?=Yii::$app->homeUrl?>user/index">View All</a></li>
                    </ul>
                </li>
                <?php }
                if(Yii::$app->user->can('distributor/index')){?>
                <li>
                    <a href="#"><i class="fa fa-user-circle"></i> <span class="nav-label">Distributor</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?=Yii::$app->homeUrl?>distributor/index">All Distributor</a></li>
                    </ul>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?=Yii::$app->homeUrl?>distributor/create">Add New</a></li>
                    </ul>
                </li>
                    <li>
                        <a href="<?=Yii::$app->homeUrl ?>orders/insure-request"><i class="fa fa-th-large"></i> <span class="nav-label">Insurance Request</span></a>
                    </li>
                <?php }
                if(Yii::$app->user->can('setting')){?>
                <li>
                    <a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Setting</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?=Yii::$app->homeUrl?>rbac/">User Roles</a></li>
                        
                    </ul>
                </li>
                <?php } ?>

            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" action="search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="Search Something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span style="font-size: 30px; font-weight: bold; color: #1ab394;  " class="m-r-sm text-muted welcome-message">Welcome to EMI</span>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <div class="dropdown-messages-box">
                                    <a class="dropdown-item float-left" href="profile.html">
                                        <img alt="image" class="rounded-circle" src="img/a7.jpg">
                                    </a>
                                    <div class="media-body">
                                        <small class="float-right">46h ago</small>
                                        <strong>Mike Loreipsum</strong> started following<strong>Monica Smith</strong>. <br>
                                        <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <div class="dropdown-messages-box">
                                    <a class="dropdown-item float-left" href="profile.html">
                                        <img alt="image" class="rounded-circle" src="img/a4.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="float-right text-navy">5h ago</small>
                                        <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                        <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <div class="dropdown-messages-box">
                                    <a class="dropdown-item float-left" href="profile.html">
                                        <img alt="image" class="rounded-circle" src="img/profile.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <small class="float-right">23h ago</small>
                                        <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                        <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <div class="text-center link-block">
                                    <a href="mailbox.html" class="dropdown-item">
                                        <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="mailbox.html" class="dropdown-item">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                        <span class="float-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <a href="profile.html" class="dropdown-item">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                        <span class="float-right text-muted small">12 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <a href="grid_options.html" class="dropdown-item">
                                    <div>
                                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                        <span class="float-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <div class="text-center link-block">
                                    <a href="notifications.html" class="dropdown-item">
                                        <strong>See All Alerts</strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="<?=Yii::$app->homeUrl?>site/logout">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>
		
	

        <div class="row wrapper border-bottom white-bg page-heading">

            <div class="col-lg-10">
                <h2> <?= Html::encode($this->title) ?></h2>
                <?=
                Breadcrumbs::widget([
                    'tag'=>'ol',
                    'options' => ['class' => 'breadcrumb'],
                    'activeItemTemplate' => '<li class="breadcrumb-item active"><strong>{link}</strong></li>',
                    'itemTemplate' => "<li class='breadcrumb-item'><a>{link}</a></li>\n",
                    'homeLink' => [
                        'label' => Yii::t('yii', 'Home'),
                        'url' => Yii::$app->homeUrl,
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>

            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox-content p-xl">

                <?= $content ?>
            </div>


        </div>



        <div class="footer">
            <div class="float-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> &copy;Maalik Creative Engineers 2019
            </div>
        </div>



    </div>

</div>
</body>

<?php $this->endBody() ?>

</html>
<?php $this->endPage() ?>
