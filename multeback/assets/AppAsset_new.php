<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace multeback\assets;

use yii\web\AssetBundle;


/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'theme/css/bootstrap.min.css',
        'theme/font-awesome/css/font-awesome.css',
        'theme/css/plugins/summernote/summernote-bs4.css',
        'theme/css/animate.css',
        'theme/css/style.css',
        'jstree/dist/themes/default/style.min.css',
        'css/site.css',
        'theme/css/plugins/slick/slick-theme.css',
        'theme/css/plugins/slick/slick.css',
        'theme/css/plugins/dataTables/datatables.min.css',
        'theme/css/plugins/footable/footable.core.css'
    ];
    public $js = [
        'theme/js/plugins/dataTables/datatables.min.js',
        'theme/js/plugins/dataTables/dataTables.bootstrap4.min.js',
        'theme/js/plugins/footable/footable.all.min.js',
        'theme/js/popper.min.js',
        'theme/js/bootstrap.js',
        'theme/js/plugins/metisMenu/jquery.metisMenu.js',
        'theme/js/plugins/slimscroll/jquery.slimscroll.min.js',
        'theme/js/inspinia.js',
        'theme/js/plugins/pace/pace.min.js',
        'theme/js/plugins/summernote/summernote-bs4.js',
        'js/bootbox.min.js',
        'jstree/dist/jstree.min.js',
        'js/custom.js',
        'theme/js/plugins/slick/slick.min.js'


    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
