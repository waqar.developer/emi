<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var multebox\models\CustomerDocuments $model
 */

$this->title = 'Create Customer Documents';
$this->params['breadcrumbs'][] = ['label' => 'Customer Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-documents-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
