<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var multebox\models\CustomerDocuments $model
 */

$this->title = 'Update Customer Documents: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Customer Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="customer-documents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
