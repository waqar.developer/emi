<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\helpers\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .table-bordered {
        border: 9px solid #EBEBEB;
    }


    legend{
        margin-left: 3%;
        font-weight: bold;
    }
    .form-control[disabled], fieldset[disabled] .form-control, .btn[disabled] {
        cursor: not-allowed;
    }
</style>




<div class="payments-form">

    <?php
            $customer = \multebox\models\Customer::find()->where(['=','id',$model->customer_id])->one();
    ?>

        <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="receipt-header receipt-header-mid">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-left">

                        <div class="receipt-right">
                            <h5><small>Bill Number : <?= $model->id ?></small></h5>
                            <p><b>Type :</b> Regular Customer</p>
                            <p><b>Mobile :</b> <?= '03069091792'; ?></p>
                            <p><b>Email :</b> <?= 'Ali@gmail.com'?></p>
                            <p><b>Address :</b> <?= 'G-7/1 Islamabad Pakistan' ?></p>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Discount</th>
                        <th>Expiry Date</th>
                    </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td class="col-md-6"><strong>Installment Charges</strong></td>
                            <td class="col-md-3"><?= $model->installment_amonut ?></td>
                            <td class="col-md-3">0</td>
                            <td>  <?php  echo  '23-jan-2019'?></td>

                        </tr>

                        <tr>
                            <td class="col-md-8"><strong>Other Charges</strong></td>
                            <td class="col-md-2"><?= 0 ?></td>
                            <td class="col-md-2"><?= 0 ?></td>
                        </tr>



                    <tr>
                        <td colspan="3" class="text-right">
                            <p>
                                <strong>Total Amount: </strong>
                            </p>
                        </td>
                        <td>
                            <p>
                                <strong> <?=$model->installment_amonut ?></strong>
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="receipt-header receipt-header-mid receipt-footer">
                    <div class="col-xs-8 col-sm-12 col-md-12 text-left">
                        <div class="receipt-right">
                            <p><b>Payment Date :</b> <?= date('d-m-Y')?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 paymentsection">

                <form class="form-horizontal" method="POST" action="<?=Yii::$app->homeUrl?>payments/payments/pay-now?id=<?=$model->id?>" role="form">
                    <fieldset>
                        <legend>Payment</legend>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="card-holder-name">Total Amount:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control rounded" value="<?=$model->installment_amonut ?>" disabled name="total-amount" id="total-amount" placeholder="Total Amount">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="card-number">Amount Paid</label>
                            <div class="col-sm-10">
                                <input type="text" required onkeyup="getremain(this);" class="form-control rounded" name="total-paid" id="total-paid" placeholder="Amount to be Paid">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="card-number">Amount Remain</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control rounded" disabled name="total-remain" id="total-remain" placeholder="0">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="expiry-month">Mode Of Payment</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control rounded" disabled name="total-remain" id="mode" placeholder="Cash">
                            </div>
                        </div>

                        <input type="hidden" name="_csrf" id="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                        <div class="form-group">
                            <div class="col-sm-10">
                                <button type="submit" id="Button" class="btn btn-success btn-rounded btn-block" disabled>Pay Now</button>
                            </div>
                        </div>
                    </fieldset>
                </form>

        </div>
    </div>


</div>
<script>
    function getremain(v){
        var paid = parseInt($(v).val());
        paid = paid || 0
        var total = parseInt(<?= (int)$model->installment_amonut?>);
        total = total || 0
        // alert(paid+'||'+total);
        var remain = parseInt(paid) - parseInt(total);
        var val= $('#total-remain').val(remain);
        if(parseInt(paid) < parseInt(total)){
            document.getElementById("Button").disabled = true;
            $("#Button").removeClass("btn btn-primary btn-rounded btn-block");
            $("#Button").addClass("btn btn-success btn-rounded btn-block");


        }
        else{
            document.getElementById("Button").disabled = false;
            $("#Button").removeClass("btn btn-success btn-rounded btn-block");
            $("#Button").addClass("btn btn-primary btn-rounded btn-block");


        }
    }
</script>