<?php

namespace multeback\modules\payments\controllers;

use Yii;
use multebox\models\PaymentPlanTemplate;
use multebox\models\PaymentPlanTemplateDetail;
use multebox\models\search\PaymentPlanTemplateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PaymentPlanTemplateController implements the CRUD actions for PaymentPlanTemplate model.
 */
class PaymentPlanTemplateController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentPlanTemplate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentPlanTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaymentPlanTemplate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PaymentPlanTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PaymentPlanTemplate();

        $model->created_by = Yii::$app->user->id;
        $model->created_at = date("Y-m-d H:i:s");

        if ($model->load(Yii::$app->request->post())) {

            $model->save();
            $paymentPlanDetail = new PaymentPlanTemplateDetail();
            $paymentPlanDetail->payment_plan_template_id = $model->id;
            $paymentPlanDetail->format = $_POST['price-type'];
            $paymentPlanDetail->payment_amount = $_POST['initial_payment'];
            $paymentPlanDetail->interval = '';
            $paymentPlanDetail->type = 'initial';
            $paymentPlanDetail->created_by = Yii::$app->user->id;
            $paymentPlanDetail->created_at = date("Y-m-d H:i:s");
                if($paymentPlanDetail->save()){

                }
                else{
                    echo '<pre>';
                    echo print_r($paymentPlanDetail);
                    echo '<pre>';
                    exit;
                }

            foreach ($_POST['payment_amount'] as $key => $value) {

                $paymentPlanDetail = new PaymentPlanTemplateDetail();
                $paymentPlanDetail->payment_plan_template_id = $model->id;
                $paymentPlanDetail->format = $_POST['price-type'];
                $paymentPlanDetail->payment_amount = $value;
                $paymentPlanDetail->interval = $_POST['duration'][$key].'-'.$_POST['period'][$key];
                $paymentPlanDetail->type = 'installment';
                $paymentPlanDetail->created_by = Yii::$app->user->id;
                $paymentPlanDetail->created_at = date("Y-m-d H:i:s");
                if($paymentPlanDetail->save()){

                }else{
                    echo '<pre>';
                    echo print_r($paymentPlanDetail);
                    echo '</pre>';
                }


            }
            return $this->redirect(['view', 'id' => $model->id]);


        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PaymentPlanTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->updated_by = Yii::$app->user->id;
            $model->updated_at = date("Y-m-d H:i:s");
            if($model->save()){

                PaymentPlanTemplateDetail::deleteAll(['=','payment_plan_template_id',$model->id]);
                $paymentPlanDetail = new PaymentPlanTemplateDetail();
                $paymentPlanDetail->payment_plan_template_id = $model->id;
                $paymentPlanDetail->format = $_POST['price-type'];
                $paymentPlanDetail->payment_amount = $_POST['initial_payment'];
                $paymentPlanDetail->interval = '';
                $paymentPlanDetail->type = 'initial';
                $paymentPlanDetail->created_by = $model->created_by;
                $paymentPlanDetail->created_at = $model->created_at;
                $paymentPlanDetail->updated_by = Yii::$app->user->id;
                $paymentPlanDetail->updated_at = date("Y-m-d H:i:s");


                if($paymentPlanDetail->save())
                {

                }
                else{
                    echo '<pre>';
                    echo print_r($paymentPlanDetail);
                    echo '<pre>';
                    exit;
                }
                foreach ($_POST['payment_amount'] as $key => $value) {

                    $paymentPlanDetail = new PaymentPlanTemplateDetail();
                    $paymentPlanDetail->payment_plan_template_id = $model->id;
                    $paymentPlanDetail->format = $_POST['price-type'];
                    $paymentPlanDetail->payment_amount = $value;
                    $paymentPlanDetail->interval = $_POST['duration'][$key].'-'.$_POST['period'][$key];
                    $paymentPlanDetail->type = 'installment';
                    $paymentPlanDetail->created_by = $model->created_by;
                    $paymentPlanDetail->created_at = $model->created_at;
                    $paymentPlanDetail->updated_by = Yii::$app->user->id;
                    $paymentPlanDetail->updated_at = date("Y-m-d H:i:s");
                    $paymentPlanDetail->save();

                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
            else{

                echo '<pre>';
                print_r($model);
                echo '</pre>';exit;
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PaymentPlanTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaymentPlanTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentPlanTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentPlanTemplate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
