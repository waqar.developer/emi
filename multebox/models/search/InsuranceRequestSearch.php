<?php

namespace multebox\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use multebox\models\InsuranceRequest;

/**
 * InsuranceRequestSearch represents the model behind the search form of `app\models\InsuranceRequest`.
 */
class InsuranceRequestSearch extends InsuranceRequest
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'order_id', 'modal_no', 'serial_no', 'IMEI1', 'IEMI2', 'status', 'created_by', 'updated_by'], 'integer'],
            [['prod_image', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InsuranceRequest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'order_id' => $this->order_id,
            'modal_no' => $this->modal_no,
            'serial_no' => $this->serial_no,
            'IMEI1' => $this->IMEI1,
            'IEMI2' => $this->IEMI2,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'prod_image', $this->prod_image]);

        return $dataProvider;
    }
}
