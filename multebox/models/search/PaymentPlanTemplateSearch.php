<?php

namespace multebox\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use multebox\models\PaymentPlanTemplate;

/**
 * PaymentPlanTemplateSearch represents the model behind the search form about `multebox\models\PaymentPlanTemplate`.
 */
class PaymentPlanTemplateSearch extends PaymentPlanTemplate
{
    public function rules()
    {
        return [
            [['id', 'bank_id', 'created_by', 'updated_by'], 'integer'],
            [['plan_name', 'plan_description', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = PaymentPlanTemplate::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'bank_id' => $this->bank_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'plan_name', $this->plan_name])
            ->andFilterWhere(['like', 'plan_description', $this->plan_description]);

        return $dataProvider;
    }
}
